package com.example.tefa.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.tefa.R
import com.example.tefa.base.BaseActivity
import com.example.tefa.databinding.ActivityMainBinding
import com.example.tefa.ui.help.HelpFragment
import com.example.tefa.ui.home.HomeFragment
import com.example.tefa.ui.profile.ProfileFragment
import com.example.tefa.utils.IntentKey
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var avatar: String

    private val homeFragment = HomeFragment()
    private val helpFragment = HelpFragment()
    private val profileFragment = ProfileFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initIntent()
        initView()
        initObserver()
        initAction()
    }

    private fun initIntent() {
        // Get all your intent here
        avatar = intent.getStringExtra(IntentKey.KEY_USER).toString()

    }

    private fun initView() {
        // Set your default UI Logic
        with(binding) {
            bnvPage.setOnItemSelectedListener { item ->
                when (item.itemId) {
                    R.id.bottom_home -> {
                        loadFragment(homeFragment)
                        true
                    }
                    R.id.bottom_help -> {
                        loadFragment(helpFragment)
                        true
                    }
                    R.id.bottom_profile -> {
                        loadFragment(profileFragment)
                        true
                    }
                    else -> {
                        false
                    }
                }
            }
            bnvPage.selectedItemId = R.id.bottom_home
        }
    }

    private fun initObserver() {
        // Init LiveData Observer here
    }

    private fun initAction() {
        // Set interaction between user and UI here
    }

    private fun loadFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.flContainerPage, fragment)
            .commit()
    }

    companion object {
        private const val TAG = "MainActivity"
    }
}