package com.example.tefa.ui.hans

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.tefa.R
import com.example.tefa.ui.hans.fragment.HomeFragment
import com.example.tefa.ui.hans.fragment.LibraryFragment
import com.example.tefa.ui.hans.fragment.SettingsFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainHansActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_hans)

        val homeFragment = HomeFragment()
        val libraryFragment = LibraryFragment()
        val settingsFragment = SettingsFragment()

        makeCurrentFragment(homeFragment)

        val bottom_navigation = findViewById<BottomNavigationView>(R.id.bottom_navigation)

        bottom_navigation.setOnItemReselectedListener {
            when (it.itemId) {
                R.id.menu_home -> makeCurrentFragment(homeFragment)
                R.id.menu_notification -> makeCurrentFragment(libraryFragment)
                R.id.menu_setting -> makeCurrentFragment(settingsFragment)
            }

            true
        }
    }

    private fun makeCurrentFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fram_nav, fragment)
            commit()
        }

    }
}