package com.example.tefa.utils

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.example.tefa.R

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun ImageView.load(url: String) {
    Glide.with(this.context)
        .load(url)
        .into(this)
}

fun ViewPager.autoScroll(interval: Long, listDots: MutableList<ImageView>, context: Context) {

    val handler = Handler(Looper.getMainLooper())
    var scrollPosition = 0

    val runnable = object : Runnable {
        override fun run() {
            val count = adapter?.count ?: 0
            setCurrentItem(scrollPosition++ % count, true)

            handler.postDelayed(this, interval)
        }
    }

    listDots[0].setImageDrawable(ContextCompat.getDrawable(context, R.drawable.indicator_dots_active))

    addOnPageChangeListener(object: ViewPager.OnPageChangeListener {
        override fun onPageSelected(position: Int) {
            scrollPosition = position + 1
            listDots.forEach {
                it.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.indicator_dots_notactive))
            }
            listDots[position].setImageDrawable(ContextCompat.getDrawable(context, R.drawable.indicator_dots_active))
        }

        override fun onPageScrollStateChanged(state: Int) {

        }

        override fun onPageScrolled(
            position: Int,
            positionOffset: Float,
            positionOffsetPixels: Int
        ) {

        }
    })

    handler.post(runnable)
}

fun LinearLayoutCompat.makeBannerIndicator(pagerAdapter: PagerAdapter, listDots: MutableList<ImageView>, context: Context) {
    for (index in 1..pagerAdapter.count) {
        val pageIndicator = ImageView(this.context)
        val params = LinearLayoutCompat.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )

        params.setMargins(8, 0, 8, 0)
        pageIndicator.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.indicator_dots_notactive))

        this.addView(pageIndicator, params)
        listDots.add(pageIndicator)
    }
}